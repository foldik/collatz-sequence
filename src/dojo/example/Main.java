package dojo.example;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		int number = 201;
		System.out.println("Collatz sequence of " + number);
		
		List<Integer> sequence = CollatzSequence.createCollatzSequence(number);
		System.out.println(sequence);
	}
}
