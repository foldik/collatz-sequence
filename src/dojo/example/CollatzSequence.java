package dojo.example;

import java.util.ArrayList;
import java.util.List;

public class CollatzSequence {

	public static List<Integer> createCollatzSequence(int number) {
		validateNumber(number);
		List<Integer> sequence = new ArrayList<>();
		int n = number;
		while (n != 1) {
			sequence.add(n);
			n = calcNextNumber(n);
		}
		sequence.add(n);
		return sequence;
	}

	private static void validateNumber(int number) {
		if (number < 1) {
			throw new IllegalArgumentException("Number must be greater than 1");
		}
	}

	private static int calcNextNumber(int number) {
		if (number % 2 == 0) {
			return number / 2;
		} else {
			return 3 * number + 1;
		}
	}

}
